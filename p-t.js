function price1_changed() {
  var price = 6400;
  if ( document.SPForm.freeradius1.checked ) { price = price +  900; }
  if ( document.SPForm.asterisk1.checked   ) { price = price +  900; }
  if ( document.SPForm.thirdlane1.checked  ) { price = price + 2695; }
  if ( price >= 10000 ) { price = addCommas(price); }
  setprice(document.getElementById('price1'), price);
}

function price2_changed() {
  var price = 8400;
  if ( document.SPForm.freeradius2.checked ) { price = price +  900; }
  if ( document.SPForm.asterisk2.checked   ) { price = price +  900; }
  if ( document.SPForm.thirdlane2.checked  ) { price = price + 2695; }
  if ( price >= 10000 ) { price = addCommas(price); }
  setprice(document.getElementById('price2'), price);
}

function price3_changed() {
  var price = 12900;
  if ( document.SPForm.freeradius3.checked ) { price = price +  900; }
  if ( document.SPForm.asterisk3.checked   ) { price = price +  900; }
  if ( document.SPForm.thirdlane3.checked  ) { price = price + 2695; }
  if ( price >= 10000 ) { price = addCommas(price); }
  setprice(document.getElementById('price3'), price);
}

var fadecols = new Array('#FFFF00','#FFFF18','#FFFF20','#FFFF28','#FFFF30','#FFFF38','#FFFF40','#FFFF48','#FFFF50','#FFFF58','#FFFF60','#FFFF68','#FFFF70','#FFFF78','#FFFF80','#FFFF88','#FFFF90','#FFFF98','#FFFFA0','#FFFFA8','#FFFFB0','#FFFFB8','#FFFFC0','#FFFFC8','#FFFFD0','#FFFFD8','#FFFFE0','#FFFFE8','#FFFFF0','#FFFFF8','#FFFFFF');

var fadehash = new Array;
for ( var i = 0; i < (fadecols.length-1); i++ ) {
  fadehash[ fadecols[i] ] = fadecols[i+1];
}

var faderInterval = false;

function setprice(element, price) {
  element.innerHTML = price;
  element.style.backgroundColor = '#FFFF00';
  element.setAttribute('data-backgroundColor', '#FFFF00' );
  if ( ! faderInterval ) {
    faderInterval = setInterval( 'dofade()', 125 );
  }
}

function dofade() {

  var didnothing = 0;

  for ( var i = 1; i < 4; i++ ) {

    var element = document.getElementById('price'+(i+''));
    var bgc = element.getAttribute('data-backgroundColor');

    if ( bgc && bgc != '#FFFFFF' ) {
      var newcolor = fadehash[ bgc ];
      element.setAttribute('data-backgroundColor', newcolor );
      element.style.backgroundColor = newcolor;
    } else {
      didnothing++;
    }

  }

  if ( didnothing == 3 ) {
    clearInterval( faderInterval );
    faderInterval = false;
  }

}

function addCommas(nStr)
{
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
}
